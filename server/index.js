const http = require("http"); // import http module
const fs = require("fs"); // import file system module

const port = 8000;

http.createServer((req, res) => {
      switch (req.url) {
        case "/":
          req.url = "index.html";
          break;
        case "/cars":
          req.url = "searchcar.html";
          break;
      }
      let path = "public/" + req.url;
      fs.readFile(path, (err, data) => {
        res.writeHead(200);
        res.end(data);
      });
})
.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});